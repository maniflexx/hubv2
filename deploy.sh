#!/bin/bash


echo "==> Building solution ..."
yarn build

echo "==> Deploying into Production region..."
aws s3 sync ./dist s3://app.maniflexx.com

#echo ""
echo "Finished Successfully"
