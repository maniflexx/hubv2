import BasicLayout from './layouts/BasicLayout';
import UserLayout from './layouts/UserLayout';
import Dashboard from './routes/Dashboard/Dashboard';
import Clients from './routes/Clients/Clients';
import Workforce from './routes/Employees/Workforce';
import Unloading from './routes/Unloading/Unloading';
import Login from './routes/User/Login';

const data = [
  {
    component: BasicLayout,
    layout: 'BasicLayout',
    name: 'Home', // for breadcrumb
    path: '',
    children: [
      {
        name: 'Dashboard',
        icon: 'dashboard',
        path: 'dashboard',
        component: Dashboard,
      },
      {
        name: 'Clients',
        icon: 'contacts',
        path: 'clients',
        component: Clients,
      },
      {
        name: 'Employees',
        icon: 'team',
        path: 'employees',
        component: Workforce,
      },
      {
        name: 'Unloading',
        path: 'unloading',
        icon: 'scan',
        component: Unloading,
      },
    ],
  },
  {
    component: UserLayout,
    layout: 'UserLayout',
    children: [{
      name: 'user',
      icon: 'user',
      path: 'user',
      children: [{
        name: 'login',
        path: 'login',
        component: Login,
      }],
    }],
  },
];

export function getNavData() {
  return data;
}

export default data;
