// ContainerBilling.options.default
// ContainerBilling.options
export const BillingOpts = [
  { value: 'SlipSheet', description: 'Slip Sheet' },
  { value: 'Palletised', description: 'Palletised' },
  { value: 'LooseUnpack', description: 'Loose Unpack' },
];

export const BillingOptsDefault = BillingOpts[0];
