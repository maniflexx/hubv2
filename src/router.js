import React from 'react';
import { Router, Route, Switch, Redirect } from 'dva/router';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import BasicLayout from './layouts/BasicLayout';
import UserLayout from './layouts/UserLayout';
import { ErrorPage } from './routes/Exception/404';
import { auth as authApi } from './services';

function RouterConfig({ history }) {
  return (
    <LocaleProvider locale={enUS}>
      <Router history={history}>
        <Switch>
          <Route
            path="/user"
            component={UserLayout}
          />

          <Route
            path="/"
            render={props => (
              !authApi.isTokenValid() ? (
                <Redirect to="/user/login" />
              ) : (
                <BasicLayout {... props} />
              ))}
          />

          <Route
            component={ErrorPage}
          />
        </Switch>
      </Router>
    </LocaleProvider>
  );
}

export default RouterConfig;
