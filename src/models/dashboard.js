import { routerRedux } from 'dva/router';
import { monitoring as monitoringApi } from '../services';

export default {
  namespace: 'dashboard',

  state: {
    metrics: [],
    isLoading: true,
  },

  effects: {
    *invokeRefreshMonitoring(_, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(monitoringApi.refreshMonitoring);
      if (response instanceof Error) {
        yield put({
          type: 'login/error',
          payload: {
            error: 'Error found. Please login again',
          },
        });
        yield put(routerRedux.push('/user/login'));
      } else {
        yield put({
          type: 'saveMetrics',
          payload: response,
        });
        yield put({
          type: 'changeLoading',
          payload: false,
        });
      }
    },
  },

  reducers: {
    saveMetrics(state, action) {
      return {
        ...state,
        metrics: action.payload,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        isLoading: action.payload,
      };
    },
  },
};
