import { routerRedux } from 'dva/router';
import {
  auth as authApi,
  clients as clientsApi,
} from '../services';

export default {
  namespace: 'login',

  state: {
    status: undefined,
    exception: null,
  },

  effects: {
    *validateToken(_, { call, put }) {
      const isValid = yield call(authApi.isTokenValid);
      if (!isValid) {
        yield put({
          type: 'logout',
        });
      }
    },
    *accountSubmit({ payload }, { call, put }) {
      yield put({
        type: 'changeSubmitting',
        payload: true,
      });
      const response = yield call(authApi.userLogin, payload);
      if (response instanceof Error) {
        yield put({
          type: 'changeLoginStatus',
          payload: 'error',
        });
      } else {
        // cache jwt token
        yield call(authApi.cacheToken, response.token);
        // clear all old caches, just in case user didnt log out
        yield call(clientsApi.clearLoggedEmployerCache);
        yield put(routerRedux.push('/dashboard'));
      }
      yield put({
        type: 'changeSubmitting',
        payload: false,
      });
    },
    *logout(_, { call, put }) {
      yield put({
        type: 'changeLoginStatus',
        payload: {
          status: false,
        },
      });
      yield call(authApi.clearLoggedUserCache);
      yield call(clientsApi.clearLoggedEmployerCache);
      yield put(routerRedux.push('/user/login'));
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      return {
        ...state,
        status: payload,
        type: 'account',
        exception: null,
      };
    },
    changeSubmitting(state, { payload }) {
      return {
        ...state,
        submitting: payload,
        exception: null,
      };
    },
    error(state, { payload }) {
      return {
        ...state,
        exception: payload.error,
      };
    },
  },
};
