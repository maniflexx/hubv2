import moment from 'moment';
import 'moment/locale/en-au';
import { BillingOptsDefault } from '../constants/unloading.constants';

import {
  clients as clientsApi,
  unloading as unloadingApi,
} from '../services';


moment.locale('en-au');

// const mockContainers = [
//   { id: 1, requestor:'MAINFREIGHT', containerNumber: 'QRP7899', containerStatus: 'SEALED'},
//   { id: 2, requestor:'MAINFREIGHT', containerNumber: 'QRP7845', containerStatus: 'UNSEALED'},
//   { id: 3, requestor:'MAINFREIGHT', containerNumber: 'QRP7847', containerStatus: 'COMPLETED'},
// ];
export default {
  namespace: 'unloading',

  state: {
    containers: [],
    containersFiltered: [],
    isLoading: true,
    // filters
    filterContainerStatus: 'all',
    filterClients: 'all',
    filterDate: moment(),

    // add new container
    newContainerModalVisible: false,
    contracts: [],
    containerNumber: '',
    retailCompanyIdSelected: null,
    billingOptionSelected: BillingOptsDefault.value,
    retailCompanyNameSelected: '',
    dueDate: moment(),
    filterDateFrom: null,
    filterDateTo: null,
    uploadConfirmLoading: false,
    containerSize: '20ft',
    files: [],

    // complete container
    containerCheckListModalVisible: false,
  },


  effects: {
    *fetchContainers(_, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      ////console.log('Getting clients ...');
      yield put({
        type: 'fetchContracts',
      });
      //console.log('Getting containers ...');
      const today = moment().format('YYYY-MM-DD');
      const response = yield call(unloadingApi.queryActivities, { start: today, end: today });
      //const activities = response.map(sm => sm.shippingManifest);
      const containers = [];
      //console.log('fetchContainers - response: ', response);
      let clientActivity;
      let shippingManifest;
      let container;
      if (response && response.length > 0) {
        for (let i = 0; i < response.length; i++) {
          clientActivity = response[i];
          shippingManifest = response[i].shippingManifest;
          for (let j = 0; j < shippingManifest.length; j++) {
            container = shippingManifest[j];
            let status;
            switch (container.status) {
              case 'NEW':
              case 'ASSIGNED':
                status = 'SEALED';
                break;
              case 'UNLOADING':
                status = 'UNSEALED';
                break;
              case 'COMPLETED':
                status = 'COMPLETED';
                break;
              default:
                status = 'UNKNOWN';
            }
            const newContainer = {
              id: clientActivity.activityId,
              requestor: clientActivity.requestorCompanyName.toUpperCase(),
              containerNumber: container.containerNumber,
              containerStatus: status,
            };
            containers.push(newContainer);
          }
        }
      }
      yield put({
        type: 'applyFilters',
        payload: {
          containers,
        },
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *applyFilters({ payload }, { select, put }) {
      let filterClients = yield select(state => state.unloading.filterClients);
      let filterStatus = yield select(state => state.unloading.filterContainerStatus);
      let containers = yield select(state => state.unloading.containers);

      //console.log('>> containers: ', containers);

      if (payload.filterClients) {
        //console.log('overriding filterClients to: ', payload.filterClients);
        filterClients = payload.filterClients;
      }
      if (payload.filterStatus) {
        //console.log('overriding filterStatus to: ', payload.filterStatus);
        filterStatus = payload.filterStatus;
      }
      if (payload.containers) {
        //console.log('overriding containers to: ', payload.containers);
        containers = payload.containers;
      }

      //console.log(`Containers being filtered by: filterClient:${filterClients} filterStatus:${filterStatus} to containers:`, containers);
      const filteredContainers = containers
        .filter(c => c.requestor.toUpperCase() === filterClients.toUpperCase() || filterClients.toUpperCase() === 'ALL')
        .filter((c) => {
          return (
            (filterStatus === 'completed' && c.containerStatus.toUpperCase() === 'COMPLETED') ||
            (filterStatus === 'notcompleted' && c.containerStatus.toUpperCase() !== 'COMPLETED') ||
            (filterStatus === 'all')
          );
        });
      //console.log('containers: ', filteredContainers);

      yield put({
        type: '_filterContainers',
        payload: {
          containersFiltered: filteredContainers,
          filterContainerStatus: filterStatus,
          filterClients,
          containers,
        },
      });
    },
    *handleFilterContainerStatus({ payload }, { put, select }) {
      const containers = yield select(state => state.unloading.containers);
      const filterClients = yield select(state => state.unloading.filterClients);
      const filterStatus = payload.filterContainerStatus;
      yield put({
        type: 'applyFilters',
        payload: {
          containers,
          filterClients,
          filterStatus,
        },
      });
    },
    *handleFilterClient({ payload }, { put, select }) {
      const containers = yield select(state => state.unloading.containers);
      const filterClients = payload.retailCompanyName;
      const filterStatus = yield select(state => state.unloading.filterContainerStatus);
      yield put({
        type: 'applyFilters',
        payload: {
          containers,
          filterClients,
          filterStatus,
        },
      });
    },
    *fetchContracts(_, { call, put }) {
      //console.log('*fetchContracts invoked');
      const response = yield call(clientsApi.fetchAllClients);
      let { contracts } = response;
      if (contracts === 'undefined') {
        contracts = [];
      }
      //console.log('* contracts - ', contracts);
      //console.log('*fetchContracts end - OK');
      yield put({
        type: '_saveContracts',
        payload: contracts,
      });
    },
    *handleContractChangeSelect({ payload }, { put }) {
      const { retailCompanyId, retailCompanyName } = payload;
      yield put({
        type: 'contractChangeSelect',
        payload: { retailCompanyId, retailCompanyName },
      });
    },
    *handleBillingOptionSelect({ payload }, { put }) {
      const billingOptionSelected = payload.billingOpt;
      yield put({
        type: 'billingOptionSelected',
        payload: { billingOptionSelected },
      });
    },
    *handleContractChangeDatePicker({ payload }, { put }) {
      const { filterDateFrom, filterDateTo } = payload;
      const dueDateFormated = moment(payload.dueDate, 'DD/MM/YYYY');
      yield put({
        type: '_contractChangeDatePicker',
        payload: { dueDate: dueDateFormated, filterDateFrom, filterDateTo },
      });
    },
    *handleOpenNewContainerModal(_, { put }) {
      yield put({
        type: '_handleOpenNewContainerModal',
      });
    },
    *handleSubmitNewContainer(_, { call, put, select }) {
      //console.log('*handleSubmitNewContainer');
      const requestorId = yield select(state => state.unloading.retailCompanyIdSelected);
      const containerNumber = yield select(state => state.unloading.containerNumber);
      const containerSize = yield select(state => state.unloading.containerSize);
      const dueDate = yield select(state => state.unloading.dueDate);
      const billingOptionSelected = yield select(state => state.unloading.billingOptionSelected);
      const unloadingDate = dueDate.format('YYYY-MM-DD');


      const data = {
        requestorId,
        unloadingDate,
        container: {
          containerNumber,
          containerSize,
          meta: {
            billingType: billingOptionSelected,
          },
        },
      };
      const submitNewContainerResp = yield call(unloadingApi.submitNewContainer, data);
      yield put({
        type: '_cleanSubmitNewContainer',
      });
      // refresh
      yield put({
        type: 'fetchContainers',
      });
    },
    *handleOnCancelNewContainer(_, { call, put }) {
      yield put({
        type: '_cleanSubmitNewContainer',
      });
    },
    *deleteContainer({ payload }, { call, put }) {
      ////console.log('[*deleteContainer] payload:', payload);
      const data = {
        activityId: payload.activityId,
        containerNumber: payload.containerNumber,
      };
      yield call(unloadingApi.removeContainer, data);
      // refresh
      yield put({
        type: 'fetchContainers',
      });
    },
    *completeContainer({ payload }, { call, put }) {
      //console.log('[*completeContainer] payload:', payload);

      yield call(unloadingApi.completeContainer, payload);
      yield put({
        type: '_hideContainerCheckListModal',
      });
      // refresh
      yield put({
        type: 'fetchContainers',
      });
    },
    *handleRemoveFiles({ payload }, { call, put }) {
      //console.log('handleRemoveFiles');
      const { index } = payload;
      yield put({
        type: 'onHandleRemoveFiles',
        payload: {
          index,
        },
      });
    },
    *handleOnContainerNumberChange({ payload }, { call, put }) {
      //console.log('action creator: handleOnContainerNumberChange invoked');
      yield put({
        type: 'onContainerNumberChange',
        payload: { containerNumber: payload.containerNumber },
      });
    },
    *handleOnContainerSizeChange({ payload }, { call, put }) {
      yield put({
        type: 'onContainerSizeChange',
        payload: { containerSize: payload.containerSize },
      });
    },
    // Container Operations
    *unsealContainer({ payload }, { call, put }) {
      //console.log('action: <*unsealContainer> invoked', payload.container);
      const { id, containerNumber } = payload.container;
      const data = {
        activityId: id,
        containerNumber,
      };
      //console.log('* unsealing container');
      yield call(unloadingApi.unsealContainer, data);

      //console.log('* refreshing containers');
      // refresh
      yield put({
        type: 'fetchContainers',
      });
    },
    *showContainerCheckListModal({ payload }, { call, put }) {
      //console.log('action: <showContainerCheckListModal> invoked. Payload=', payload);
      yield put({
        type: '_showContainerCheckListModal',
      });
    },
    *hideContainerCheckListModal(_, { call, put }) {
      //console.log('action: <hideContainerCheckListModal> invoked.');
      yield put({
        type: '_hideContainerCheckListModal',
      });
    },
  },

  reducers: {
    listContainers(state, action) {
      return {
        ...state,
        containers: action.payload,
        containersFiltered: action.payload,
      };
    },
    _filterContainers(state, action) {
      return {
        ...state,
        containers: action.payload.containers,
        containersFiltered: action.payload.containersFiltered,
        filterContainerStatus: action.payload.filterContainerStatus,
        filterClients: action.payload.filterClients,
      };
    },
    _handleOpenNewContainerModal(state, action) {
      return {
        ...state,
        newContainerModalVisible: true,

      };
    },
    _cleanSubmitNewContainer(state, action) {
      return {
        ...state,
        newContainerModalVisible: false,
        containerNumber: '',
        retailCompanyIdSelected: null,
        retailCompanyNameSelected: '',
        billingOptionSelected: BillingOptsDefault.value,
        dueDate: moment(),
        filterDateFrom: null,
        filterDateTo: null,
        uploadConfirmLoading: false,
        containerSize: '20ft',
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        isLoading: action.payload,
      };
    },
    contractChangeSelect(state, action) {
      return {
        ...state,
        retailCompanyIdSelected: action.payload.retailCompanyId,
        retailCompanyNameSelected: action.payload.retailCompanyName,
      };
    },
    billingOptionSelected(state, action) {
      const { billingOptionSelected } = action.payload;
      return {
        ...state,
        billingOptionSelected,
      };
    },
    _contractChangeDatePicker(state, action) {
      return {
        ...state,
        dueDate: action.payload.dueDate,
        filterDateFrom: action.payload.filterDateFrom,
        filterDateTo: action.payload.filterDateTo,
      };
    },
    onCancelNewContainer(state, action) {
      return {
        ...state,
        newContainerModalVisible: false,
        files: [],
        dueDate: moment(),
        retailCompanyIdSelected: null,
        retailCompanyNameSelected: '',
      };
    },
    onHandleRemoveFiles(state, action) {
      const { index } = action.payload;
      const files = state.files.filter((x, i) => i !== index);
      return {
        ...state,
        files,
      };
    },
    onContainerNumberChange(state, action) {
      //console.log('reducer: onContainerNumberChange invoked');
      return {
        ...state,
        containerNumber: action.payload.containerNumber,
      };
    },
    onContainerSizeChange(state, action) {
      return {
        ...state,
        containerSize: action.payload.containerSize,
      };
    },
    _unsealContainer(state, action) {
      //console.log('reducer: <_unsealContainer> invoked');
      return {
        ...state,
        containers: action.payload.containers,
        containersFiltered: action.payload.containersFiltered,
      };
    },
    _showContainerCheckListModal(state, action) {
      return {
        ...state,
        containerCheckListModalVisible: true,
      };
    },
    _hideContainerCheckListModal(state, action) {
      return {
        ...state,
        containerCheckListModalVisible: false,
      };
    },
    _saveContracts(state, action) {
      return {
        ...state,
        contracts: action.payload,
      };
    },
  },
};
