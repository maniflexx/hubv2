import {
  workforce as workforceApi,
} from '../services';

export default {
  namespace: 'workforce',

  state: {
    unfilteredWorkforce: [],
    filteredWorkforce: [],
    filterDropdownVisible: false,
    isLoading: false,
    // create employee modal
    createEmployeeModalVisible: false,
    // edit employee modal
    editEmployeeModalVisible: false,

    selectedEmployeeToEdit: {
      id: '',
      username: '',
      password: '',
      fullname: '',
      email: '',
      phone: '',
      profile: '',
    },
  },

  effects: {
    *loadAllEmployees(_, { call, put }) {
      yield put({ type: '_hideAllEmployeeModals' });
      yield put({ type: '_startLoading' });
      const response = yield call(workforceApi.fetchAllEmployees);
      yield put({
        type: '_stopLoadingOK',
        payload: {
          workforce: response,
        },
      });
    },
    *showCreateEmployeeModal(_, { put }) {
      yield put({ type: '_showCreateEmployeeModal', payload: true });
    },
    *hideCreateEmployeeModal(_, { put }) {
      yield put({ type: '_hideAllEmployeeModals' });
    },
    *createNewEmployee({ payload }, { call, put }) {
      // yield put({ type: '_startLoading' });
      yield call(workforceApi.createEmployee, payload);
      yield put({ type: 'loadAllEmployees' });
    },
    *deleteEmployee({ payload }, { call, put }) {
      const params = {
        employeeId: payload.employeeId,
      };
      yield call(workforceApi.deleteEmployee, params);
      yield put({ type: 'loadAllEmployees' });
    },
    *showEditEmployeeModal({ payload }, { put }) {
      yield put({
        type: '_showEditEmployeeModal',
        payload,
      });
    },
    *cancelEditEmployee(_, { put }) {
      yield put({ type: '_hideAllEmployeeModals' });
    },
    *saveEditEmployee({ payload }, { put, call }) {
      const params = payload.employee;
      yield call(workforceApi.editEmployee, params);
      yield put({ type: 'loadAllEmployees' });
    },
  },

  reducers: {
    _startLoading(state, action) {
      return {
        ...state,
        isLoading: true,
      };
    },
    _stopLoadingOK(state, action) {
      return {
        ...state,
        isLoading: false,
        unfilteredWorkforce: action.payload.workforce,
        filteredWorkforce: action.payload.workforce,
      };
    },
    _showCreateEmployeeModal(state, action) {
      return {
        ...state,
        createEmployeeModalVisible: action.payload,
      };
    },
    _showEditEmployeeModal(state, action) {
      const selectedEmployeeToEdit = action.payload.employee;
      return {
        ...state,
        editEmployeeModalVisible: true,
        selectedEmployeeToEdit: { ...selectedEmployeeToEdit },
      };
    },
    _hideAllEmployeeModals(state, action) {
      return {
        ...state,
        createEmployeeModalVisible: false,
        editEmployeeModalVisible: false,
        selectedEmployeeToEdit: {
          employerId: '',
          username: '',
          password: '',
          fullname: '',
          email: '',
          phone: '',
          profile: '',
        },
      };
    },
  },
};
