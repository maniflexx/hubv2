import { clients as clientsApi } from '../services';

export default {
  namespace: 'clients',

  state: {
    listClients: [],
    isLoading: true,
    createFormIsVisible: false,
    editFormIsVisible: false,
    clientform: {
      companyName: '', type: 'retail', email: '', phone: '', website: '',
    },
    clienteditform: {
      companyId: '', companyName: '', type: 'retail', email: '', phone: '', website: '', status: '',
    },
  },

  effects: {
    *loadAllClients(_, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });

      const response = yield call(clientsApi.fetchAllClients);
      yield call(clientsApi.cacheLoggedEmployer, response);
      yield put({
        type: 'saveClients',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *editClient({ payload }, { call, put }) {
      yield put({
        type: 'hideAllModals',
      });
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      yield call(clientsApi.clearLoggedEmployerCache);
      yield call(clientsApi.editClient, payload);
      const response = yield call(clientsApi.fetchAllClients);
      yield put({
        type: 'saveClients',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
      yield put({
        type: 'clearForms',
      });
      yield call(clientsApi.cacheLoggedEmployer, response);
    },
    *deleteClient({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      yield call(clientsApi.deleteClient, payload);
      yield call(clientsApi.clearLoggedEmployerCache);
      const response = yield call(clientsApi.fetchAllClients);
      yield put({
        type: 'saveClients',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
      yield call(clientsApi.cacheLoggedEmployer, response);
    },
    *createClient({ payload }, { put, call }) {
      yield put({
        type: 'hideAllModals',
      });
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      yield call(clientsApi.createClient, payload);
      yield call(clientsApi.clearLoggedEmployerCache);
      const response = yield call(clientsApi.fetchAllClients);
      yield put({
        type: 'saveClients',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
      yield put({
        type: 'clearForms',
      });
      yield call(clientsApi.cacheLoggedEmployer, response);
    },
    *showCreateClientModal(_, { put }) {
      yield put({
        type: 'showCreateClientModalReducer',
        payload: true,
      });
    },
    *showEditClientModal({ payload }, { put }) {
      yield put({
        type: 'showEditClientModalReducer',
        payload,
      });
    },
    *handleChangeCreateForm({ payload }, { put }) {
      yield put({
        type: 'handleChangeCreateFormReducer',
        payload,
      });
    },
    *handleChangeEditForm({ payload }, { put }) {
      yield put({
        type: 'handleChangeEditFormReducer',
        payload,
      });
    },
    *handleCancelOptInModal(_, { put }) {
      yield put({
        type: 'hideAllModals',
      });
      yield put({
        type: 'clearForms',
      });
    },
  },

  reducers: {
    saveClients(state, { payload }) {
      return {
        ...state,
        listClients: payload.contracts,
      };
    },
    showCreateClientModalReducer(state, { payload }) {
      return {
        ...state,
        createFormIsVisible: payload,
      };
    },
    showEditClientModalReducer(state, { payload }) {
      const { editFormIsVisible, clienteditform } = payload;
      return {
        ...state,
        editFormIsVisible,
        clienteditform,
      };
    },
    changeLoading(state, { payload }) {
      return {
        ...state,
        isLoading: payload,
        createFormIsVisible: false,
        editFormIsVisible: false,
      };
    },
    handleChangeCreateFormReducer(state, { payload }) {
      const { index, value } = payload;
      const newClientForm = state.clientform;
      newClientForm[index] = value;
      return {
        ...state,
        clientform: newClientForm,
      };
    },
    handleChangeEditFormReducer(state, { payload }) {
      const { index, value } = payload;
      const newClientForm = state.clienteditform;
      newClientForm[index] = value;
      return {
        ...state,
        clienteditform: newClientForm,
      };
    },
    hideAllModals(state) {
      return {
        ...state,
        createFormIsVisible: false,
        editFormIsVisible: false,
      };
    },
    clearForms(state) {
      const clearClientForm = {
        companyName: '', type: 'retail', email: '', phone: '', website: '',
      };
      const clearClientEditForm = {
        companyId: '', companyName: '', type: 'retail', email: '', phone: '', website: '', status: '',
      };
      return {
        ...state,
        clientform: clearClientForm,
        clienteditform: clearClientEditForm,
      };
    },
  },
};
