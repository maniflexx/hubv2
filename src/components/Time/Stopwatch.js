import React, { Component } from 'react';

const formattedSeconds = (sec) => {
  let hours = Math.floor(sec / 3600);
  let minutes = Math.floor((sec - (hours * 3600)) / 60);
  let seconds = sec - (hours * 3600) - (minutes * 60);

  if (hours < 10) { hours = `0${hours}` }
  if (minutes < 10) { minutes = `0${minutes}` }
  if (seconds < 10) { seconds = `0${seconds}`}
  return `${hours}h ${minutes}m ${seconds}s`
}


class Stopwatch extends Component {
  constructor(props) {
    super(props)
    this.state = {
      seconds: this.props.seconds
    }
    this.incrementer = null;
  }

  componentDidMount() {
    this.incrementer = setInterval( () =>
      this.setState({
        seconds: this.state.seconds + 1
      })
    , 1000);
  }

  componentWillUnmount() {
    clearInterval(this.incrementer);
  }

  render() {
    return(
      <p>{formattedSeconds(this.state.seconds)}</p>
    )
  }
}

export default Stopwatch;
