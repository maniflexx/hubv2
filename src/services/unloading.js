import { authenticatedRequest, authenticatedRequestWithNotification } from './common/request';

export async function submitNewContainer(params) {
  return authenticatedRequestWithNotification('/api/container', {
    method: 'POST',
    body: {
      method: 'post',
      requestorId: params.requestorId,
      unloadingDate: params.unloadingDate,
      container: {
        containerNumber: params.container.containerNumber,
        containerSize: params.container.containerSize,
        meta: {
          billingType: params.container.meta.billingType,
        },
      },
    },
  }, 'Error found while creating a container', `Container: ${params.container.containerNumber} created successfully`);
}

export async function removeContainer(params) {
  const response =
   authenticatedRequestWithNotification('/api/container', {
     method: 'DELETE',
     body: {
       method: 'delete',
       activityId: params.activityId,
       containerNumber: params.containerNumber,
     },
   }, 'Error found while deleting a container', `Container: ${params.containerNumber} removed successfully`);
  return response;
}

export async function unsealContainer(params) {
  return authenticatedRequestWithNotification('/api/container/unseal', {
    method: 'POST',
    body: {
      method: 'post',
      activityId: params.activityId,
      containerNumber: params.containerNumber,
    },
  }, 'Error found while unsealing a container', `Container: ${params.containerNumber} unseal successfully`);
}

export async function completeContainer(params) {
  const {
    activityId,
    clientCommodity,
    comments,
    containerNumber,
    finalPalletQuantity,
    hasDriverHandedKeys,
    hasPhotosTakenForContainers,
    hasPhotosTakenForProduct,
    isContainerDamaged,
    isContainerStandInUse,
    isDockerOrRollerdoorDamaged,
    isProductDamaged,
    isWheelChookInUse,
    location,
    reasonForDamaged,
    sealNo,
    teamMembers,
  } = params;
  return authenticatedRequestWithNotification('/api/container/complete', {
    method: 'POST',
    body: {
      method: 'post',
      activityId,
      clientCommodity,
      comments,
      containerNumber,
      finalPalletQuantity,
      hasDriverHandedKeys,
      hasPhotosTakenForContainers,
      hasPhotosTakenForProduct,
      isContainerDamaged,
      isContainerStandInUse,
      isDockerOrRollerdoorDamaged,
      isProductDamaged,
      isWheelChookInUse,
      location,
      reasonForDamaged,
      sealNo,
      teamMembers,
    },
  }, 'Error found while marking the container as completed', `Container: ${params.containerNumber} completed successfully`);
}

export async function queryActivities(params) {
  const { start, end } = params;
  return authenticatedRequest(`/api/companies/activity?start=${start}&end=${end}`);
}
