import { authenticatedRequest } from './common/request';

export async function refreshMonitoring() {
  return authenticatedRequest('/api/companies/metrics');
}
