import {
  authenticatedRequest,
  authenticatedRequestWithNotification,
} from './common/request';
import * as cache from './common/cache';

// "companyId":"589b1ad346e0fb00018613a4",
// "companyName":"2kSolutions",
// "contracts":[{ "companyId":"589b1ad046e0fb000186139c",
//                "companyName":"Kmart",
//                "website":"www.kmart.com",
//                "email":"contact@kmart.com",
//                "phone":"999999",
//                "status":"active"}, ... ]
export const CACHE_KEY_LOGGED_EMPLOYER = '[CACHE - LOGGED_EMPLOYER]';

export async function clearLoggedEmployerCache() {
  // console.log(`${CACHE_KEY_LOGGED_EMPLOYER} - Clearing cache`);
  cache.clear(CACHE_KEY_LOGGED_EMPLOYER);
}

export async function cacheLoggedEmployer(clients) {
  // console.log(`${CACHE_KEY_LOGGED_EMPLOYER} - Saving data into cache`);
  cache.write(CACHE_KEY_LOGGED_EMPLOYER, clients);
}

export async function fetchAllClients() {
  // console.log('[clients.js] fetchAllClients ... ');
  if (cache.isEmpty(CACHE_KEY_LOGGED_EMPLOYER)) {
    // console.log(`${CACHE_KEY_LOGGED_EMPLOYER} - Miss`);
    const data = await authenticatedRequest('/api/contracts');
    cacheLoggedEmployer(data);
    return data;
  }
  // console.log(`${CACHE_KEY_LOGGED_EMPLOYER} - Hit`);
  return cache.read(CACHE_KEY_LOGGED_EMPLOYER);
}

export async function createClient(params) {
  const { companyName, type, email, phone, website } = params;
  return authenticatedRequest('/api/companies', {
    method: 'POST',
    body: {
      method: 'post',
      name: companyName,
      type,
      email,
      phone,
      website,
    },
  });
}

export async function editClient(params) {
  const {
    companyId,
    companyName,
    email,
    phone,
    website,
  } = params;
  return authenticatedRequestWithNotification(`/api/companies/${companyId}`, {
    method: 'PUT',
    body: {
      name: companyName,
      type: 'retail',
      email,
      phone,
      website,
    },
  }, true, 'Client modified successfully');
}

export async function deleteClient(params) {
  const { clientId } = params;
  const loggedEmployer = cache.read(CACHE_KEY_LOGGED_EMPLOYER);
  const unloadingCoId = loggedEmployer.companyId;

  return authenticatedRequestWithNotification('/api/contracts', {
    method: 'DELETE',
    body: {
      method: 'delete',
      unloadingCoId,
      retailCoId: clientId,
    },
  }, true, 'Client deleted successfully');
}
