import * as jwt from 'jsonwebtoken';
import * as cache from './common/cache';
import request, { authenticatedRequest } from './common/request';

export const CACHE_KEY_TOKEN = 'id_token';
export const CACHE_KEY_LOGGED_USER = 'logged_user'; // keys: 'id', 'username', 'fullname', 'profile', 'employerId', 'email', 'phone'

export async function clearLoggedUserCache() {
  cache.clear(CACHE_KEY_LOGGED_USER);
  cache.clear(CACHE_KEY_TOKEN);
}

/**
 * Customer Token util funcs
 */
export function isLoggedIn() {
  return !(cache.read(CACHE_KEY_TOKEN) === null);
}

export function getJwtToken() {
  return cache.read(CACHE_KEY_TOKEN);
}

export function isTokenValid() {
  if (!isLoggedIn) return false;
  // user is logged in
  const token = getJwtToken();
  const decodedToken = jwt.decode(token);
  if (!decodedToken) return false;
  const dateNow = new Date().getTime();
  const dateNowStd = Math.ceil(dateNow / 1000);
  // console.log('decodedToken: ', decodedToken);
  // console.log(`isValid if: ${decodedToken.exp} > ${dateNowStd}`);
  const isValid = (decodedToken.exp > dateNowStd);
  // console.log('Token valid? ', isValid);
  return isValid;
}

/**
 * User/Login
 */
export async function userLogin(params) {
  return request('/sts/issue', {
    method: 'POST',
    body: params,
  }, false);
}

export async function cacheToken(token) {
  // console.log('auth.js - cache jwt token ...');
  // lets clear this cache before writing
  cache.clear(CACHE_KEY_TOKEN);
  cache.write(CACHE_KEY_TOKEN, token);
}

export async function queryCurrentUser() {
  if (cache.isEmpty(CACHE_KEY_LOGGED_USER)) {
    return authenticatedRequest('/api/employees');
  }
  return cache.read(CACHE_KEY_LOGGED_USER);
}

export async function cacheCurrentUserData(currentUserData) {
  cache.write(CACHE_KEY_LOGGED_USER, currentUserData);
}
