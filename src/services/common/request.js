import fetch from 'dva/fetch';
import { notification } from 'antd';
import { getJwtToken } from '../auth';

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

export function authenticatedRequest(url, options, notifyError) {
  const newOptions = { ...options };
  const jwt = getJwtToken();
  newOptions.headers = { Authorization: `Bearer ${jwt}`, ...newOptions.headers };
  return request(url, newOptions, notifyError);
}

export function authenticatedRequestWithNotification(url, options, notifyError, messageOnSuccess) {
  const newOptions = { ...options };
  const jwt = getJwtToken();
  newOptions.headers = { Authorization: `Bearer ${jwt}`, ...newOptions.headers };
  return request(url, newOptions, notifyError, messageOnSuccess);
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(path, options, notifyError = true, messageOnSuccess = null) {
  const defaultOptions = {
    credentials: 'include',
  };
  const newOptions = { ...defaultOptions, ...options };
  if (newOptions.method === 'POST' ||
      newOptions.method === 'PUT' ||
      newOptions.method === 'DELETE') {
    newOptions.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      ...newOptions.headers,
    };
    newOptions.body = JSON.stringify(newOptions.body);
  }

  let apiUrl;
  if (process.env.NODE_ENV === 'production') {
    apiUrl = 'http://api.maniflexx.com:8080';
  } else {
    apiUrl = 'http://localhost:8080'; //'http://api.maniflexx.com:8080';
  }

  // console.log(`** ${apiUrl}${path} **`);
  const endpointUrl = `${apiUrl}${path}`;
  return fetch(endpointUrl, newOptions)
    .then(checkStatus)
    .then((response) => {
      if (messageOnSuccess !== null) {
        notification.success({
          message: messageOnSuccess,
        });
      }
      //console.log("response.json:", response.json());
      return response.json()
        .then((data) => {
          return data;
        })
        .catch((error) => {
          //console.log('[WARN] response was empty. Returning empty object');
          return JSON.stringify({});
        });
    })
    .catch((error) => {
      // console.log('error code:', error.response.status);
      switch (error.response.status) {
        case 422:
          notification.warning({
            message: 'Warning',
            description: 'Entity already exists',
          });
          break;
        case 500:
          // notification.error({
          //   message: 'Session Expired',
          //   description: 'Re-login is required',
          // });
          break;
        default:
          notification.error({
            message: error.name,
            description: `${endpointUrl} - ${error.message}`,
          });
      }
      return error;
    });
}
