export function write(key, value) {
  if (value instanceof Error) {
    return;
  }
  let newValue = value;
  if (typeof value === 'object') {
    newValue = JSON.stringify(newValue);
  }
  localStorage.setItem(key, newValue);
}

export function clear(key) {
  localStorage.removeItem(key);
}

export function isEmpty(key) {
  const result = read(key);
  return (result === null || result === 'undefined');
}

export function readById(key, subkey) {
  const value = read(key);
  if (typeof value === 'object') {
    return value[subkey];
  }
  return null;
}

export function read(key) {
  const value = localStorage.getItem(key);

  try {
    return JSON.parse(value);
  } catch (e) {
    return value;
  }
}
