import { authenticatedRequest, authenticatedRequestWithNotification } from './common/request';

export async function fetchAllEmployees() {
  return authenticatedRequest('/api/companies/workforce');
}

export async function createEmployee(params) {
  const {
    username,
    password,
    fullname,
    email,
    phone,
    profile,
  } = params;
  return authenticatedRequestWithNotification('/api/companies/employee', {
    method: 'POST',
    body: {
      method: 'post',
      username,
      password,
      fullname,
      email,
      phone,
      profile,
    },
  }, 'Error found while creating employee', 'Employee created successfully');
}

export async function deleteEmployee(params) {
  const { employeeId } = params;
  return authenticatedRequestWithNotification(`/api/companies/employee/${employeeId}`, {
    method: 'DELETE',
    body: {
      method: 'delete',
    },
  }, 'Error found while deleting the employee', 'Employee deleted successfully');
}

export async function editEmployee(params) {
  const {
    employeeId,
    username,
    password,
    fullname,
    email,
    phone,
    profile,
  } = params;
  return authenticatedRequestWithNotification(`/api/employees/${employeeId}`, {
    method: 'PUT',
    body: {
      method: 'put',
      username,
      password,
      fullname,
      email,
      phone,
      profile,
    },
  }, 'Error found while deleting the employee', 'Employee deleted successfully');
}

