import request from './common/request';

export async function query() {
  return request('/api/users');
}
