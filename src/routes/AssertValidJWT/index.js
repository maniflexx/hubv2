import React, { Component } from 'react';
import { connect } from 'dva';
/**
 * <AssertValidJWT redirectTo='/user/logout' />
 */
@connect(state => ({
  login: state.login,
}))
export default class AssertValidJWT extends Component {
  assertToken = () => {
    // console.log('[AssertValidJWT] - checking if token is valid ...');
    this.props.dispatch({
      type: 'login/validateToken',
    });
  }
  render() {
    this.assertToken();
    return null;
  }
}
