import React from 'react';
import { Modal, Form } from 'antd';
import { LinkedComponent } from 'valuelink';
import { Input, Select } from 'valuelink/tags';

const FormItem = Form.Item;


class WorkforceEditForm extends LinkedComponent {
  constructor(props) {
    super(props);
    this.onCancel = props.onCancel;
    this.onOK = props.onOK;
    this.state = {
      employeeId: props.employee.id,
      username: props.employee.username || '',
      password: props.employee.password || '',
      fullname: props.employee.fullname || '',
      email: props.employee.email || '',
      phone: props.employee.phone || '',
      profile: props.employee.profile || 'UNLOADER',
    };
  }

  handleOnSave = () => {
    this.onOK(this.state);
  }

  handleOnCancel = () => {
    this.onCancel();
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      username: newProps.employee.username || '',
      password: newProps.employee.password || '',
      fullname: newProps.employee.fullname || '',
      email: newProps.employee.email || '',
      phone: newProps.employee.phone || '',
      profile: newProps.employee.profile || 'UNLOADER',
    });
  }

  render() {
    const links = this.linkAll(); // wrap all state members in links
    const profiles = ['UNLOADER', 'TEAM LEADER', 'ADMIN']; // ROLE_WEB_ADMIN,ROLE_WEB_DASHBOARD,ROLE_ONSITE_UNLOAD,ROLE_ONSITE_TEAML

    return (
      <Modal
        visible
        title="Edit Employee"
        okText="Save"
        cancelText="Cancel"
        onOk={this.handleOnSave}
        onCancel={this.handleOnCancel}
      >
        <Form layout="vertical">
          <FormItem label="Full Name">
            <Input valueLink={links.fullname} />
          </FormItem>
          <FormItem label="Username">
            <Input valueLink={links.username} />
          </FormItem>
          <FormItem label="Password">
            <Input valueLink={links.password} />
          </FormItem>
          <FormItem label="Email">
            <Input valueLink={links.email} />
          </FormItem>
          <FormItem label="Phone Number">
          	<Input valueLink={links.phone} />
          </FormItem>
          <FormItem label="Profiles">
    				<Select valueLink={links.profile}>
              {profiles.map((p) => (<option key={p} value={p}>{p}</option>) )}
						</Select>
					</FormItem>
        </Form>
      </Modal>
    );
  }
}

export default WorkforceEditForm;
