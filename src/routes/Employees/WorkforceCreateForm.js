import React from 'react';
import { Modal, Form } from 'antd';
import { LinkedComponent } from 'valuelink';
import { Input, Select } from 'valuelink/tags';

const FormItem = Form.Item;


class WorkforceCreateForm extends LinkedComponent {
  constructor(props) {
    super(props);
    this.onCancel = props.onCancel;
    this.onOK = props.onOK;
    this.state = {
      username: '',
      password: '',
      fullname: '',
      email: '',
      phone: '',
      profile: 'UNLOADER',
    };
  }

  onSubmit = () => {
    this.onOK(this.state);
  }

  render() {
    const links = this.linkAll(); // wrap all state members in links
    const profiles = ['UNLOADER', 'TEAM LEADER', 'ADMIN']; // ROLE_WEB_ADMIN,ROLE_WEB_DASHBOARD,ROLE_ONSITE_UNLOAD,ROLE_ONSITE_TEAML

    return (
      <Modal
        visible
        title="New Employee"
        okText="Create"
        cancelText="Cancel"
        onOk={this.onSubmit}
        onCancel={this.onCancel}
      >
        <Form layout="vertical">
          <FormItem label="Full Name">
            <Input valueLink={links.fullname} />
          </FormItem>
          <FormItem label="Username">
            <Input valueLink={links.username} />
          </FormItem>
          <FormItem label="Password">
            <Input valueLink={links.password} />
          </FormItem>
          <FormItem label="Email">
            <Input valueLink={links.email} />
          </FormItem>
          <FormItem label="Phone Number">
          	<Input valueLink={links.phone} />
          </FormItem>
          <FormItem label="Profiles">
    				<Select valueLink={links.profile}>
              {profiles.map((p) => (<option key={p} value={p}>{p}</option>) )}
						</Select>
					</FormItem>
        </Form>
      </Modal>
    );
  }
}

export default WorkforceCreateForm;
