import React, { Component } from 'react';
import { connect } from 'dva';
import { Table, Button, Icon, Popconfirm, Card } from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import WorkforceCreateForm from './WorkforceCreateForm';
import WorkforceEditForm from './WorkforceEditForm';
import './Workforce.less';

const ButtonGroup = Button.Group;

@connect(state => ({
  workforce: state.workforce,
}))
export default class Workforce extends Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'workforce/loadAllEmployees',
    });
  }

  handleOnShowCreateEmployeeModal = () => {
    this.props.dispatch({
      type: 'workforce/showCreateEmployeeModal',
    });
  }

  handleOnCancelNewEmployee = () => {
    this.props.dispatch({
      type: 'workforce/hideCreateEmployeeModal',
    });
  }

  handleOnCreateNewEmployee = (form) => {
    this.props.dispatch({
      type: 'workforce/createNewEmployee',
      payload: form,
    });
  }

  handleOnDeleteEmployee = (employeeId) => {
    this.props.dispatch({
      type: 'workforce/deleteEmployee',
      payload: {
        employeeId,
      },
    });
  }

  handleOnShowEditEmployeeModal = (employee) => {
    this.props.dispatch({
      type: 'workforce/showEditEmployeeModal',
      payload: {
        employee,
      },
    });
  }

  handleOnUpdateEmployee = (employee) => {
    this.props.dispatch({
      type: 'workforce/saveEditEmployee',
      payload: { employee },
    });
  }

  handleOnCancelEditEmployee = () => {
    this.props.dispatch({
      type: 'workforce/cancelEditEmployee',
    });
  }


  render() {
    const {
      filteredWorkforce,
      isLoading,
      createEmployeeModalVisible,
      editEmployeeModalVisible,
      selectedEmployeeToEdit,
    } = this.props.workforce;

    //console.log('Workforce component received prop: editEmployeeModalVisible=', editEmployeeModalVisible);
    const locale = { emptyText: 'No Data' };

    const columns = [{
      title: 'Full Name',
      dataIndex: 'fullname',
      key: 'fullname',
    },
    {
      title: 'Username',
      key: 'username',
      dataIndex: 'username',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Phone',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Profile',
      key: 'profile',
      dataIndex: 'profile',
    },
    {
      title: 'Actions',
      key: 'actions',
      render: (text, record) => (
        <ButtonGroup>
          <Button onClick={() => this.handleOnShowEditEmployeeModal(record)}>Edit</Button>
          <Popconfirm
            title="Are you sure you want to delete this employee?"
            okText="Yes"
            cancelText="No"
            onConfirm={() => this.handleOnDeleteEmployee(record.id)}
          >
            <Button type="danger">Delete</Button>
          </Popconfirm>
        </ButtonGroup>
      ),
    }];

    return (
      <PageHeaderLayout title="Employees">
        <Card
          title="Employee list"
          extra={<Button type="primary" onClick={this.handleOnShowCreateEmployeeModal}><Icon type="plus-circle-o" />New Employee</Button>}
          bordered={false}
        >
          <Table
            dataSource={filteredWorkforce}
            columns={columns}
            loading={isLoading}
            locale={locale}
            rowKey="id"
          />

          { createEmployeeModalVisible ? (
            <WorkforceCreateForm
              onOK={this.handleOnCreateNewEmployee}
              onCancel={this.handleOnCancelNewEmployee}
            />
            ) : null
          }

          { editEmployeeModalVisible ? (
            <WorkforceEditForm
              onOK={this.handleOnUpdateEmployee}
              onCancel={this.handleOnCancelEditEmployee}
              employee={selectedEmployeeToEdit}
            />
            ) : null
          }
        </Card>
      </PageHeaderLayout>
    );
  }
}
