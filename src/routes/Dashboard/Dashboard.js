import React, { Component } from 'react';
import { connect } from 'dva';
import { Row, Col, Table, LocaleProvider, Progress, Button, Card } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import { Pie } from '../../components/Charts';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import StaticDuration from '../../components/Time/StaticDuration';
import Stopwatch from '../../components/Time/Stopwatch';
import styles from './Dashboard.less';

@connect(state => ({
  dashboard: state.dashboard,
}))

export default class Dashboard extends Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'dashboard/invokeRefreshMonitoring',
    });
  }

  totalsPieData = () => {
    const completedContainers = this.props.dashboard.metrics.reduce((acc, now) => (now.completedSuccess || 0) + acc, 0);
    const totalContainers = this.props.dashboard.metrics.reduce((acc, now) => now.totalContainers + acc, 0);
    const pendingContainers = totalContainers - completedContainers;
    return [
      { x: 'completed', y: completedContainers },
      { x: 'pending', y: pendingContainers },
    ];
  }

  render() {
    const { dashboard } = this.props;
    const {
      isLoading,
      metrics }
    = dashboard;

    const columns = [
      {
        title: 'Requestor',
        dataIndex: 'requestor.name',
      },
      {
        title: 'Progress',
        key: 'Progress',
        render: (text, record) => {
          const totalContainers = record.totalContainers || 0;
          const completedSuccess = record.completedSuccess || 0;
          const progress = Math.floor(100 * (completedSuccess / totalContainers));
          const percentage = isNaN(progress) ? 0 : progress;
          return (
            <div>
              <Progress percent={percentage} showInfo={false} />
            </div>
          );
        },
      },
      {
        title: '',
        key: 'progressnumbers',
        render: (text, record) => {
          const totalContainers = record.totalContainers || 0;
          const completedContainers = record.completedSuccess || 0;
          return (
            <div>
              {`${completedContainers}/${totalContainers}`}
            </div>
          );
        },
      },
      {
        title: 'Incidents',
        key: 'incidents',
        render: (text, record) => {
          const errors = record.errors === null ? 0 : record.errors.length;
          return (
            <p>{errors}</p>
          );
        },
      },
      {
        title: 'Duration',
        key: 'Duration',
        render: (text, record) => {
          let durationTimer = null;

          if (record.durationStart === null && record.durationEnd === null) {
            durationTimer = <p>-- : -- : --</p>;
          }

          if (record.durationStart && record.durationEnd) {
            const startTime = new Date(record.durationStart);
            const endTime = new Date(record.durationEnd);
            const seconds = (endTime.getTime() - startTime.getTime()) / 1000;
            durationTimer = <StaticDuration seconds={Math.floor(seconds)} />;
          }

          if (record.durationStart && record.durationEnd === null) {
            const startTime = new Date(record.durationStart);
            const currentTime = new Date();
            const seconds = (currentTime.getTime() - startTime.getTime()) / 1000;
            durationTimer = <Stopwatch seconds={Math.floor(seconds)} />;
          }

          return (
            <div>
              {durationTimer}
            </div>
          );
        },
      }];

    const refreshButton = (
      <Button
        type="primary"
        icon="reload"
        onClick={() => this.props.dispatch({ type: 'dashboard/invokeRefreshMonitoring' })}
      >Refresh
      </Button>
    );
    return (
      <PageHeaderLayout title="Container Unloading Realtime tracking">
        <Row gutter={12}>
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>
            <Card
              title="Detailed Progress"
              extra={refreshButton}
              bordered={false}
            >

              <LocaleProvider locale={enUS}>
                <Table
                  dataSource={metrics}
                  columns={columns}
                  loading={isLoading}
                  pagination={false}
                  rowKey={record => record.requestor.id}
                />
              </LocaleProvider>
            </Card>
          </Col>
          {/* 
          <Col xl={24} lg={24} md={24} sm={24} xs={24}>
            <Card
              title="Overall Progress"
              bordered={false}
            >
              <Pie
                hasLegend
                subTitle="Total Containers"
                total={metrics.reduce((acc, now) => now.totalContainers + acc, 0)}
                data={this.totalsPieData()}
                height={248}
                lineWidth={4}
              />
            </Card>
          </Col>
          */}
        </Row>
      </PageHeaderLayout>
    );
  }
}
