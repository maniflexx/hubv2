import React, { Component } from 'react';
import { Form, Select, DatePicker, Modal, Icon, Input, Radio } from 'antd';
import locale from 'antd/lib/date-picker/locale/en_US';

import { BillingOpts } from '../../../constants/unloading.constants';
import styles from './UploadForm.css';


const FormItem = Form.Item;
const { Option } = Select;

class UploadForm extends Component {
  onChangeBilling = (k, v) => {
    console.log(`selected key:{${k}} value:{${v}}`);
  }

  render() {
    const dateFormat = 'DD/MM/YYYY';
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    return (
      <Modal
        className={styles.verticalCenterModal}
        visible={this.props.uploadModalVisibility}
        onCancel={this.props.onCancel}
        onOk={this.props.onSubmit}
        okText="Upload"
        cancelText="Cancel"
        title="Create Container"
        confirmLoading={this.props.confirmLoading}
      >
        <Form>
          <FormItem
            {...formItemLayout}
            label="Clients"
          >
            <Select value={this.props.retailCompanyName} onSelect={this.props.onChangeSelect}>
              {this.props.contracts.map((contract) => {
                return (
                  <Option key={contract.companyId} value={contract.companyId}>{contract.companyName}</Option>
                );
              })}
            </Select>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Billing"
          >
            <Select value={this.props.billingOptionSelected} onSelect={this.props.onChangeBilling}>
              {BillingOpts.map((billingOpt) => {
                return (
                  <Option key={billingOpt.value} value={billingOpt.value}>{billingOpt.description}</Option>
                );
              })}
            </Select>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Due Date"
          >
            <DatePicker
              locale={locale}
              onChange={this.props.onChangeDatePicker}
              defaultValue={this.props.dueDate}
              format={dateFormat}
              allowClear={false}
            />
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Container Number"
          >
            <Input
              placeholder="Type here a value"
              value={this.props.containerNumber}
              onChange={this.props.onContainerNumberChange} />
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Container Size"
          >
            <Radio.Group
              value={this.props.containerSize}
              onChange={this.props.onContainerSizeChange}
            >
              <Radio.Button value="20ft">20ft</Radio.Button>
              <Radio.Button value="40ft">40ft</Radio.Button>
            </Radio.Group>
          </FormItem>
        </Form>
        { this.props.files.length ? (<p className="fileuploader-hint"><Icon type="info-circle-o" /> Press Upload to proceed with container manifest upload</p>) : ''}
      </Modal>
    );
  }
}

export default UploadForm;
