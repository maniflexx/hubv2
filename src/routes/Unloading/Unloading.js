import React, { Component } from 'react';
import { Button, Card, Col, Icon, List, Radio, Row, Select } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import 'moment/locale/en-au';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import ContainerCard from './ContainerCard';
import UploadForm from './NewContainer/UploadForm';

import styles from './Unloading.less';

const { Option } = Select;
moment.locale('en-au');

@connect(state => ({
  unloading: state.unloading,
}))
export default class Unloading extends Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'unloading/fetchContainers',
    });
  }

  // handleChangeDate = (value) => {
  //   switch (value) {
  //     case 'tomorrow':
  //       //console.log('tomorrow selected');
  //       const tomorrow = moment().add(1, 'days').format('YYYY-MM-DD');
  //       this.props.dispatch({
  //         type: 'unloading/fetchContainers',
  //         payload: { start: tomorrow, end: tomorrow }
  //       });
  //       break;
  //     case 'today':
  //       //console.log('today selected');
  //       const today = moment().format('YYYY-MM-DD');
  //       this.props.dispatch({
  //         type: 'unloading/fetchContainers',
  //         payload: { start: today, end: today }
  //       });
  //       break;
  //     default:
  //       //console.log('No implemented yet: ', value);
  //
  //   }
  // }

  handleSubmitNewContainer = (e) => {
    e.preventDefault();
    this.props.dispatch({
      type: 'unloading/handleSubmitNewContainer',
    });
  }

  handleOnCancelNewContainer = () => {
    this.props.dispatch({
      type: 'unloading/handleOnCancelNewContainer',
    });
  }

  handleChangeSelect = (value, label) => {
    this.props.dispatch({
      type: 'unloading/handleContractChangeSelect',
      payload: {
        retailCompanyId: value,
        retailCompanyName: label.props.children,
      },
    });
  }

  handleOnChangeBilling = (value, label) => {
    ////console.log(`Billing change to value: ${value}, label:`, label);
    this.props.dispatch({
      type: 'unloading/handleBillingOptionSelect',
      payload: {
        billingOpt: value,
      },
    });
  }

  handeRemoveFiles = (index) => {
    this.props.dispatch({
      type: 'unloading/handleRemoveFiles',
      payload: {
        index,
      },
    });
  }

  handleOpenNewContainerModal = () => {
    this.props.dispatch({
      type: 'unloading/handleOpenNewContainerModal',
    });
  }

  handleChangeDatePicker = (datestring) => {
    this.props.dispatch({
      type: 'unloading/handleContractChangeDatePicker',
      payload: {
        dueDate: datestring,
        filterDateFrom: datestring,
        filterDateTo: datestring,
      },
    });
  }

  handleOnContainerNumberChange = (event) => {
    const containerNumber = event.target.value;
    this.props.dispatch({
      type: 'unloading/handleOnContainerNumberChange',
      payload: {
        containerNumber,
      },
    });
  }

  handleOnContainerSizeChange = (e) => {
    const containerSize = e.target.value;
    this.props.dispatch({
      type: 'unloading/handleOnContainerSizeChange',
      payload: {
        containerSize,
      },
    });
  }

  handleFilterClient = (value, label) => {
    this.props.dispatch({
      type: 'unloading/handleFilterClient',
      payload: {
        retailCompanyId: value,
        retailCompanyName: label.props.children,
      },
    });
  }

  handleFilterContainerStatus = (e) => {
    const filterContainerStatus = e.target.value;
    this.props.dispatch({
      type: 'unloading/handleFilterContainerStatus',
      payload: { filterContainerStatus },
    });
  }

  render() {
    const {
      containersFiltered,
      isLoading,
      filterContainerStatus,
      filterClients,
      newContainerModalVisible,
      retailCompanyIdSelected,
      retailCompanyNameSelected,
      billingOptionSelected,
      contracts,
      dueDate,
      containerNumber,
      containerSize,
      files,
      uploadConfirmLoading,
    } = this.props.unloading;

    //console.log('[Unloading.js] billingOptionSelected=',billingOptionSelected);
    return (
      <div>
        <UploadForm
          uploadModalVisibility={newContainerModalVisible}
          retailCompanyId={retailCompanyIdSelected}
          retailCompanyName={retailCompanyNameSelected}
          billingOptionSelected={billingOptionSelected}
          contracts={contracts}
          dueDate={dueDate}
          files={files}
          confirmLoading={uploadConfirmLoading}
          containerNumber={containerNumber}
          containerSize={containerSize}
          onChangeSelect={this.handleChangeSelect}
          onChangeDatePicker={this.handleChangeDatePicker}
          onSubmit={this.handleSubmitNewContainer}
          onCancel={this.handleOnCancelNewContainer}
          removeFiles={this.handeRemoveFiles}
          onContainerNumberChange={this.handleOnContainerNumberChange}
          onContainerSizeChange={this.handleOnContainerSizeChange}
          onChangeBilling={this.handleOnChangeBilling}
        />

        <PageHeaderLayout
          logo={<img alt="" src="https://gw.alipayobjects.com/zos/rmsportal/nxkuOJlFJuAUhzlMTCEe.png" />}
          title="Containers"
        >
          <div className={styles.cardFilters}>
            <Card bordered>
              <Row gutter={12}>
                <Col lg={12} md={12} sm={24} xs={24}>
                  <span>Status: </span>
                  <Radio.Group
                    value={filterContainerStatus}
                    onChange={this.handleFilterContainerStatus}
                  >
                    <Radio.Button value="all">All</Radio.Button>
                    <Radio.Button value="notcompleted">Not Completed</Radio.Button>
                    <Radio.Button value="completed">Completed</Radio.Button>
                  </Radio.Group>
                </Col>
                <Col lg={12} md={12} sm={24} xs={24}>
                  <span>Client: </span>
                  <Select className={styles.filterClients} defaultValue={filterClients} onSelect={this.handleFilterClient}>
                    <Option key="all" value="all">All</Option>
                    {contracts.map((contract) => {
                      return (
                        <Option key={contract.companyId} value={contract.companyId}>{contract.companyName}</Option>
                      );
                    })}
                  </Select>
                </Col>
                {/*
              <Col lg={8} md={8} sm={14} xs={24}>
                <Button type='primary' onClick={this.handleRefresh}>
                  <Icon type="reload" /> Refresh
              </Button>
              </Col>

            <Col lg={8} md={8} sm={14} xs={24}>
                <span>Date: </span>
                <Select defaultValue="today" style={{ width: 120 }} onChange={this.handleChangeDate}>
                  <Option value="today">Today</Option>
                  <Option value="tomorrow">Tomorrow</Option>
                  <Option value="customDate">Pick a date</Option>
                </Select>
              </Col>
    */}
              </Row>
            </Card>
          </div>
          <div className={styles.cardList}>
            <List
              rowKey="id"
              loading={isLoading}
              grid={{ gutter: 24, lg: 4, md: 3, sm: 1, xs: 1 }}
              dataSource={['', ...containersFiltered]}
              renderItem={item => (item ? (
                <List.Item key={`${item.id}${item.containerNumber}`}>
                  <ContainerCard item={item} />
                </List.Item>
              ) : (
                <List.Item>
                  <Button
                    className={styles.newButton}
                    type="dashed"
                    onClick={this.handleOpenNewContainerModal}
                  >
                    <Icon type="plus" /> Add Container
                  </Button>
                </List.Item>
                )
              )}
            />
          </div>

        </PageHeaderLayout>
      </div>
    );
  }
}
