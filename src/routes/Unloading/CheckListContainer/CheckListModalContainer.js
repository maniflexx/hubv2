import { connect } from 'dva';
import React, { Component } from 'react';
import CheckListModal from './CheckListModal';

@connect(state => ({
  unloading: state.unloading,
}))
export default class CheckListModalContainer extends Component {
  constructor(props) {
    super(props);
    this.containerNo = this.props.containerNo;
    this.visible = this.props.visible;
    this.onSubmit = this.props.onSubmit;
    this.onCancel = this.props.onCancel;
  }

  componentWillReceiveProps(nextProps) {
    this.visible = nextProps.visible;
  }
  handleOnCancel = () => {
    this.onCancel();
  }


  handleOnFinish = (form) => {
    this.onSubmit(form);
  }

  render() {
    return (
      <div>
        <CheckListModal
          containerNo={this.containerNo}
          visible={this.visible}
          onCancel={this.handleOnCancel}
          onFinish={this.handleOnFinish}
        />
      </div>
    )
  }
}
