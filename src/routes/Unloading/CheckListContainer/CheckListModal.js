import { Form, Modal } from 'antd';
import React from 'react';
import { LinkedComponent } from 'valuelink';
import { Input } from 'valuelink/tags';
import styles from './CheckListModal.less';

const FormItem = Form.Item;

class CheckListModal extends LinkedComponent {
  constructor(props) {
    super(props);
    this.containerNo = this.props.containerNo;
    this.state = {
      teamMembers: '',
      clientCommodity: '',
      location: '',
      sealNo: '',
      finalPalletQuantity: '',
      isContainerStandInUse: false,
      isWheelChookInUse: false,
      hasDriverHandedKeys: false,
      isContainerDamaged: false,
      hasPhotosTakenForContainers: false,
      isDockerOrRollerdoorDamaged: false,
      isProductDamaged: false,
      hasPhotosTakenForProduct: false,
      reasonForDamaged: '',
      comments: '',
    };
  }

  onOk = () => {
    this.props.onFinish(this.state);
  }

  onCancel = () => {
    this.props.onCancel();
  }

  componentWillReceiveProps() {
    this.resetState();
  }

  resetState = () => {
    this.setState({
      teamMembers: '',
      clientCommodity: '',
      location: '',
      sealNo: '',
      finalPalletQuantity: '',
      isContainerStandInUse: false,
      isWheelChookInUse: false,
      hasDriverHandedKeys: false,
      isContainerDamaged: false,
      hasPhotosTakenForContainers: false,
      isDockerOrRollerdoorDamaged: false,
      isProductDamaged: false,
      hasPhotosTakenForProduct: false,
      reasonForDamaged: '',
      comments: '',
    });
  }

  render() {
    const links = this.linkAll(); // wrap all state members in links
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const checkboxItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 10 },
      },
    };
    return (
      <Modal
        style={{ top: 20 }}
        visible={this.props.visible}
        onCancel={this.onCancel}
        onOk={this.onOk}
        okText="Finish"
        cancelText="Cancel"
        title={`Checklist: ${this.containerNo}`}
        confirmLoading={false}
        width={720}
      >
        <div className="form-upload">
          <Form>
            <FormItem {...formItemLayout} label="Team Members:">
              <Input valueLink={links.teamMembers} size={45} width={45} />
            </FormItem>

            <FormItem {...formItemLayout} label="Client Commodity:">
              <Input valueLink={links.clientCommodity} size={45} width={45} />
            </FormItem>

            <FormItem {...formItemLayout} label="Location:">
              <Input valueLink={links.location} size={45} width={45} />
            </FormItem>

            <FormItem {...formItemLayout} label="Seal No:">
              <Input valueLink={links.sealNo} size={45} width={45} />
            </FormItem>

            <FormItem {...formItemLayout} label="Final Pallet Quantity">
              <Input valueLink={links.finalPalletQuantity} />
            </FormItem>

            <FormItem {...checkboxItemLayout} label="Is the container stand in use?">
              <Input type="checkbox" className={styles.largeCheckbox} checkedLink={links.isContainerStandInUse} />
            </FormItem>

            <FormItem {...checkboxItemLayout} label="Is the wheel chook in use?">
              <Input type="checkbox" className={styles.largeCheckbox} checkedLink={links.isWheelChookInUse} />
            </FormItem>

            <FormItem {...checkboxItemLayout} label="Has the driver handed in keys in the case of a live load/unload?">
              <Input type="checkbox" className={styles.largeCheckbox} checkedLink={links.hasDriverHandedKeys} />
            </FormItem>

            <FormItem {...checkboxItemLayout} label="Is there any visible damage to container?">
              <Input type="checkbox" className={styles.largeCheckbox} checkedLink={links.isContainerDamaged} />
            </FormItem>

            <FormItem {...checkboxItemLayout} label="Have photos been taken of damage?">
              <Input type="checkbox" className={styles.largeCheckbox} checkedLink={links.hasPhotosTakenForContainers} />
            </FormItem>

            <FormItem {...checkboxItemLayout} label="Is there any visible damage to the dock or roller door?">
              <Input type="checkbox" className={styles.largeCheckbox} checkedLink={links.isDockerOrRollerdoorDamaged} />
            </FormItem>

            <FormItem {...checkboxItemLayout} label="Damaged product?">
              <Input type="checkbox" className={styles.largeCheckbox} checkedLink={links.isProductDamaged} />
            </FormItem>

            <FormItem {...checkboxItemLayout} label="Have photos been taken of damage?">
              <Input type="checkbox" className={styles.largeCheckbox} checkedLink={links.hasPhotosTakenForProduct} />
            </FormItem>

            <FormItem {...formItemLayout} label="Reason for damage">
              <Input valueLink={links.reasonForDamaged} size={45} width={45} />
            </FormItem>

            <FormItem {...formItemLayout} label="Comments">
              <Input valueLink={links.comments} size={45} width={45} />
            </FormItem>
          </Form>
        </div>
      </Modal>
    );
  }
}

export default CheckListModal;
