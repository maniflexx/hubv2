import { Button, Card, Icon, Modal, Tag } from 'antd';
import { connect } from 'dva';
import React, { Component } from 'react';
import CheckListModalContainer from './CheckListContainer/CheckListModalContainer';
import styles from './Unloading.less';

const { confirm } = Modal;

@connect(state => ({
  unloading: state.unloading,
}))

export default class ContainerCard extends Component {
  constructor(props) {
    super(props);
    this.item = this.props.item;
    this.state = { completeModalVisible: false };
  }

  componentWillReceiveProps(nextProps) {
    this.item = nextProps.item;
  }


  showDeleteConfirm = () => {
    confirm({
      title: 'Do you want to delete this container?',
      content: 'All information about this container will be lost',
      onOk: () => {
        this.handleOnDeleteContainer(this.item);
      },
      onCancel: () => {
      },
      okType: 'danger',
    });
  }

  handleOnDeleteContainer(container) {
    this.props.dispatch({
      type: 'unloading/deleteContainer',
      payload: {
        activityId: container.id,
        containerNumber: container.containerNumber,
      },
    });
  }

  handleOnViewClick = () => {
    //console.log('View clicked on item: ', this.item);
  }

  handleOnUnsealClick = () => {
    this.props.dispatch({
      type: 'unloading/unsealContainer',
      payload: { container: this.item },
    });
  }

  handleOnComplete = (form) => {
    this.props.dispatch({
      type: 'unloading/completeContainer',
      payload: {
        activityId: this.item.id,
        clientCommodity: form.clientCommodity,
        comments: form.comments,
        containerNumber: this.item.containerNumber,
        finalPalletQuantity: form.finalPalletQuantity,
        hasDriverHandedKeys: form.hasDriverHandedKeys,
        hasPhotosTakenForContainers: form.hasPhotosTakenForContainers,
        hasPhotosTakenForProduct: form.hasPhotosTakenForProduct,
        isContainerDamaged: form.isContainerDamaged,
        isContainerStandInUse: form.isContainerStandInUse,
        isDockerOrRollerdoorDamaged: form.isDockerOrRollerdoorDamaged,
        isProductDamaged: form.isProductDamaged,
        isWheelChookInUse: form.isWheelChookInUse,
        location: form.location,
        reasonForDamaged: form.reasonForDamaged,
        sealNo: form.sealNo,
        teamMembers: form.teamMembers,
      },
    });
    this.setState({ completeModalVisible: false });
  }

  handleOnCancel = () => {
    this.props.dispatch({
      type: 'unloading/hideContainerCheckListModal',
    });
    this.setState({ completeModalVisible: false });
  }

  handleOnShowCompleteContainer = (container) => {
    this.setState({ completeModalVisible: true });
    // this.props.dispatch({
    //   type: 'unloading/showContainerCheckListModal',
    //   payload: { container: container }
    // });
  }

  renderCover = (item) => {
    if (item.containerStatus.toUpperCase() === 'COMPLETED') {
      return (<div style={{ padding: '10px' }}><Tag color="green"><Icon type="like-o" /> COMPLETED</Tag></div>);
    } else if (item.containerStatus.toUpperCase() === 'UNSEALED') {
      return (<div style={{ padding: '10px' }}><Tag color="orange"><Icon type="unlock" /> UNLOADING</Tag></div>);
    } else {
      return (<div style={{ padding: '10px' }}><Tag color="red"><Icon type="lock" /> SEALED</Tag></div>);
    }
  }

  renderViewAction = () => {
    return (
      <div onClick={this.handleOnViewClick}><Icon type="eye-o" /> View</div>
    );
  }

  renderUnsealAction = () => {
    return (
      <div onClick={this.handleOnUnsealClick}><Icon type="unlock" /> Unseal</div>
    );
  }

  renderCompleteAction = () => {
    return (
      <div onClick={this.handleOnShowCompleteContainer}><Icon type="like-o" /> Complete</div>
    );
  }

  renderEmptyAction = () => {
    return (
      <div />
    );
  }

  renderActions = () => {
    if (this.item.containerStatus.toUpperCase() === 'COMPLETED') {
      return ([]);
    } else if (this.item.containerStatus.toUpperCase() === 'UNSEALED') {
      return ([this.renderCompleteAction()]);
    } else {
      return ([this.renderUnsealAction()]);
    }
  }

  render() {
    return (
      <div>
        <CheckListModalContainer
          containerNo={this.item.containerNumber}
          visible={this.state.completeModalVisible}
          onSubmit={this.handleOnComplete}
          onCancel={this.handleOnCancel}
        />
        <Card
          hoverable
          className={styles.card}
          cover={this.renderCover(this.item)}
          actions={this.renderActions()}
        >
          <Card.Meta
            title={this.item.containerNumber}
            description={this.item.requestor}
          />
          <div style={{ position: 'absolute', top: '-15px', left: '-15px' }}>
            <Button
              type="normal"
              shape="circle"
              icon="delete"
              onClick={this.showDeleteConfirm}
            />
          </div>
        </Card>
      </div>
    );
  }
}
