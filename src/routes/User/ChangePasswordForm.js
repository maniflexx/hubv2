import React from 'react';
import { Form, Input, Icon, Button } from 'antd';
import LoginAlert from './LoginAlert';
const FormItem = Form.Item;

const ChangePasswordForm = (props) => {
	return(
		<div className="change-password-form">
			<h2>Change Password</h2>
			<LoginAlert 
				isInvalid={props.isInvalid}
				invalidMessage={props.invalidMessage}
			/>

			<Form onSubmit={props.onSubmit}>
				<FormItem>
					<Input 
						name="old_password"
						type="password"
						placeholder="Old Password"
						value={props.change_password.old_password}
						onChange={props.onChange}
						prefix={<Icon type="lock" />}
					/>
				</FormItem>
				<FormItem>
					<Input 
						name="new_password"
						type="password"
						placeholder="New Password"
						value={props.change_password.new_password}
						onChange={props.onChange}
						prefix={<Icon type="lock" />}
					/>
				</FormItem>
				<Button type="green" htmlType="submit" className="login-form-button">Change Password</Button>
			</Form>
		</div>
	);
}

ChangePasswordForm.propTypes = {
	onSubmit: React.PropTypes.func.isRequired,
	onChange: React.PropTypes.func.isRequired,
	change_password: React.PropTypes.object
}

export default ChangePasswordForm;