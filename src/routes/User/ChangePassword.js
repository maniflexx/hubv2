import React, { Component } from 'react';
import ChangePasswordForm from './ChangePasswordForm';
import { httpPost } from '../../utils';
import { setDocumentTitle } from '../../utils';

class ChangePassword extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isInvalid: false,
			invalidMessage: '',
			change_password: {
				old_password: '',
				new_password: ''
			}
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.displayError = this.displayError.bind(this);
	}

	handleChange(event) {
		const field = event.target.name;
		const change_password = this.state.change_password;
		change_password[field] = event.target.value;
		return this.setState({change_password: change_password});
	}

  handleSubmit(event) {
    event.preventDefault();
		if (!this.state.change_password.old_password && !this.state.change_password.new_password) {
			return this.displayError('Please enter your old password and new password')
		}

		if (this.state.change_password.old_password === this.state.change_password.new_password) {
			return this.displayError('Please use a different password from your old password')
		}

		const { old_password, new_password } = this.state.change_password
		httpPost(process.env.REACT_APP_API_URL + '/api/employees/access/change_password', { "oldPassword": old_password, "newPassword": new_password })
		.catch((error) => {
			console.log(error);
		})
  }

	displayError(message) {
		this.setState({
			isInvalid: true,
			invalidMessage: message
		})
	}

	componentDidMount() {
		setDocumentTitle('Change Password')
	}

	render() {
		return(
				<ChangePasswordForm
					onSubmit={this.handleSubmit}
					onChange={this.handleChange}
					isInvalid={this.state.isInvalid}
					invalidMessage={this.state.invalidMessage}
					change_password={this.state.change_password}
				/>
		);
	}
}

export default ChangePassword;
