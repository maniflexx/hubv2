import React, { Component } from 'react';
import { connect } from 'dva';
import { Table, Button, Icon, Popconfirm, Card } from 'antd';
import ClientCreateForm from './ClientCreateForm';
import ClientEditForm from './ClientEditForm';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

const ButtonGroup = Button.Group;

@connect(state => ({
  clients: state.clients,
}))
export default class Clients extends Component {
  componentDidMount() {
    this.props.dispatch({
      type: 'clients/loadAllClients',
    });
  }

  createButton = (
    <Button
      type="primary"
      onClick={() => this.props.dispatch({ type: 'clients/showCreateClientModal' })}
    >

      <Icon type="plus-circle-o" />Register New Client
    </Button>
  );

  handleShowEditModal = (id) => {
    this.props.dispatch({
      type: 'clients/showEditClientModal',
      payload: {
        clienteditform: id,
        editFormIsVisible: true,
      },
    });
  }
  handleEditClient = () => {
    const {
      companyId,
      companyName,
      email,
      phone,
      website,
    } = this.props.clients.clienteditform;
    this.props.dispatch({
      type: 'clients/editClient',
      payload: {
        companyId,
        companyName,
        email,
        phone,
        website,
      },
    });
  }

  handleCreateClient = () => {
    const { companyName, type, email, phone, website } = this.props.clients.clientform;
    this.props.dispatch({
      type: 'clients/createClient',
      payload: {
        companyName,
        type,
        email,
        phone,
        website,
      },
    });
  }

  handleDeleteClient = (clientId) => {
    this.props.dispatch({
      type: 'clients/deleteClient',
      payload: {
        clientId,
      },
    });
  }

  handleCreateFormChange = (event) => {
    this.props.dispatch({
      type: 'clients/handleChangeCreateForm',
      payload: {
        index: event.target.name,
        value: event.target.value,
      },
    });
  }

  handleEditFormChange = (event) => {
    this.props.dispatch({
      type: 'clients/handleChangeEditForm',
      payload: {
        index: event.target.name,
        value: event.target.value,
      },
    });
  }

  render() {
    const {
      listClients,
      isLoading,
      createFormIsVisible,
      editFormIsVisible,
      clientform,
      clienteditform,
    } = this.props.clients;

    const locale = {
      emptyText: 'No Data',
    };
    const columns = [
      {
        title: 'Company Name',
        key: 'companyName',
        dataIndex: 'companyName',
      },
      {
        title: 'Status',
        key: 'status',
        dataIndex: 'status',
      },
      {
        title: 'Email',
        key: 'email',
        dataIndex: 'email',
      },
      {
        title: 'Phone',
        key: 'phone',
        dataIndex: 'phone',
      },
      {
        title: 'Website',
        key: 'website',
        dataIndex: 'website',
      },
      {
        title: 'Actions',
        key: 'action',
        render: (text, record) => (
          <ButtonGroup>
            <Button onClick={() => this.handleShowEditModal(record)}>Edit</Button>
            <Popconfirm
              title="Are you sure you want to delete"
              okText="Yes"
              cancelText="No"
              onConfirm={() => this.handleDeleteClient(record.companyId)}
            >
              <Button type="danger">Delete</Button>
            </Popconfirm>
          </ButtonGroup>
        ),
      }];

    return (
      <PageHeaderLayout title="Retail Clients">
        <Card
          title="Clients list"
          extra={this.createButton}
          bordered={false}
        >
          <Table
            loading={isLoading}
            locale={locale}
            dataSource={listClients}
            columns={columns}
            pagination={false}
            rowKey="companyId"
          />

          <ClientCreateForm
            ref={this.saveFormRef}
            visible={createFormIsVisible}
            clientform={clientform}
            onCreate={this.handleCreateClient}
            onChange={this.handleCreateFormChange}
            onCancel={() => this.props.dispatch({ type: 'clients/handleCancelOptInModal' })}
          />

          <ClientEditForm
            visible={editFormIsVisible}
            clienteditform={clienteditform}
            onCreate={this.handleEditClient}
            onChange={this.handleEditFormChange}
            onCancel={() => this.props.dispatch({ type: 'clients/handleCancelOptInModal' })}
          />
        </Card>
      </PageHeaderLayout>
    );
  }
}
