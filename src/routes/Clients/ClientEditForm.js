import React from 'react';
import { Form, Input, Modal } from 'antd';

const FormItem = Form.Item;

const ClientEditForm = (
  (props) => {
    const { visible, onCancel, onCreate, onChange } = props;
    return (
      <Modal
        visible={visible}
        title="Edit Client"
        okText="Update"
        cancelText="Cancel"
        onCancel={onCancel}
        onOk={onCreate}
      >
        <Form vertical>
          <FormItem label="Name">
            <Input
              name="companyName"
              placeholder="Client Name"
              value={props.clienteditform.companyName}
              onChange={onChange}
            />
          </FormItem>
          <FormItem label="Email">
            <Input
              name="email"
              placeholder="Email Address"
              value={props.clienteditform.email}
              onChange={onChange}
            />
          </FormItem>
          <FormItem label="Phone">
            <Input
              name="phone"
              type="tel"
              placeholder="Phone"
              value={props.clienteditform.phone}
              onChange={onChange}
            />
          </FormItem>
          <FormItem label="Website">
            <Input
              name="website"
              placeholder="Website"
              value={props.clienteditform.website}
              onChange={onChange}
            />
          </FormItem>
        </Form>
      </Modal>
    );
  }
);

export default ClientEditForm;
