import React from 'react';
import { Form, Input, Modal, } from 'antd';

const FormItem = Form.Item;

const ClientCreateForm = (
  (props) => {
    const { visible, onCancel, onCreate, onChange } = props;
    return (
      <Modal
        visible={visible}
        title="New Client"
        okText="Create"
        cancelText="Cancel"
        onCancel={onCancel}
        onOk={onCreate}
      >
        <Form layout="vertical">
          <FormItem label="Name">
          	<Input
          		name="companyName"
          		placeholder="Client Name"
          		value={props.clientform.companyName}
              onChange={onChange}
          	/>
          </FormItem>
					<FormItem label="Email">
						<Input
							name="email"
							placeholder="Email Address"
							value={props.clientform.email}
						  onChange={onChange}
						/>
					</FormItem>
					<FormItem label="Phone">
						<Input
							name="phone"
							type="tel"
							placeholder="Phone"
							value={props.clientform.phone}
						  onChange={onChange}
						/>
					</FormItem>
					<FormItem label="Website">
						<Input
							name="website"
							placeholder="Website"
							value={props.clientform.website}
						  onChange={onChange}
						/>
					</FormItem>
        </Form>
      </Modal>
    );
  }
);

export default ClientCreateForm;
